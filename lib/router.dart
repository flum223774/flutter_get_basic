import 'package:flutter_get/home/detail.dart';
import 'package:flutter_get/home/home.dart';
import 'package:get/get.dart';

import 'home/binding/home_bidding.dart';

class AppRoutes {
  AppRoutes._(); //this is to prevent anyone from instantiating this object
  static final routes = [
      GetPage(name: '/', page: () => HomePage(), bindings: [
        HomeBinding()
      ], children: [
        GetPage(name: 'detail', page: () => Detail(), children: [])
      ])
    ];
}
