import 'package:get/get.dart';

class HomeProvider extends GetConnect {
  Future<Response> getUser(int id) => get('https://jsonplaceholder.typicode.com/users/$id');
}
