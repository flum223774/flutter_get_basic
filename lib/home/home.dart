import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controllers/home_controller.dart';

class HomePage extends StatelessWidget {
  // final HomeController homeController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<HomeController>(
        builder: (homeController){
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Homepage ${homeController.counter}'),
                ElevatedButton(onPressed: ()=> homeController.upCounter(), child: Text('Counter Up')),
                ElevatedButton(onPressed: ()=> homeController.fetchData(), child: Text('Fetch Data')),
                ElevatedButton(onPressed: (){
                  Get.toNamed('/detail');
                }, child: Text('Go detail'))
              ],
            ),
          );
        },
      ),
    );
  }
}
