import 'package:flutter_get/home/provider/home_provider.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  HomeProvider homeProvider = Get.find();
  // static HomeController get to => Get.find();
  RxInt _counter = 0.obs;

  int get counter => _counter.value;

  upCounter() {
    _counter +=1;
    print(_counter);
    update();
  }
  Future fetchData() async{
   Response data =  await homeProvider.getUser(1);
   print(data.body);
  }
}
