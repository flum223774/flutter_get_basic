import 'package:flutter_get/home/controllers/home_controller.dart';
import 'package:flutter_get/home/provider/home_provider.dart';
import 'package:get/get.dart';

class HomeBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(HomeProvider());
    Get.put(HomeController());
  }
}